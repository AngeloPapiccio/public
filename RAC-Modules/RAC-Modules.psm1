function Get-FSMORole {
    <#	
	.NOTES
	===========================================================================
	 Created on:   	10/12/2015 7:11 AM
	 Created by:   	Angelo Papiccio
	 Organization: 	RAC of WA
	 Filename:     	GetFSMORoles.ps1
	 Adding some new text to check if it will update
	===========================================================================
	.DESCRIPTION
	This function can be used to identify which domain controller is holding the FSMO roles
    #>
	[CmdletBinding()]
	param (
		[Parameter(ValueFromPipeline = $True)]
		[string[]]$DomainName = $env:USERDOMAIN
	)
	BEGIN {
		Import-Module ActiveDirectory -Cmdlet Get-ADDomain, Get-ADForest -ErrorAction SilentlyContinue
	}
	PROCESS {
		foreach ($domain in $DomainName) {
			Write-Verbose "Querying $domain"
			Try {
				$problem = $false
				$addomain = Get-ADDomain -Identity $domain -ErrorAction Stop
			}
			Catch {
				$problem = $true
				Write-Warning $_.Exception.Message
			}
			if (-not $problem) {
				$adforest = Get-ADForest -Identity (($addomain).forest)
				
				New-Object PSObject -Property @{
					InfrastructureMaster = $addomain.InfrastructureMaster
					PDCEmulator = $addomain.PDCEmulator
					RIDMaster = $addomain.RIDMaster
					DomainNamingMaster = $adforest.DomainNamingMaster
					SchemaMaster = $adforest.SchemaMaster
				}
			}
		}
	}
}

function Copy-GPOLinks {
	<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.119
	 Created on:   	12/04/2016 7:26 AM
	 Created by:   	Angelo Papiccio
	 Organization: 	RACWA 
	 Filename:     	Copy-GPOLinks.ps1
	===========================================================================
	This function was based on code posted on Open a Socket web site.
	.Synopsis
       Will copy all linked GPO from one OU to another OU

    .DESCRIPTION
      	This function can be used to create a testing environment by copying all the linked Group Policy Objects from
		one OU to another OU.
		
		It can also be used when doing migration of group policies between OU's

    .EXAMPLE
	     Copy-GPOLinks -SourceOU 'OU=Computers,OU=RAC,DC=rac,DC=com,DC=au' -TargetOU 'OU=Computers,OU=RAC2,DC=rac,DC=com,DC=au'

    .FUNCTIONALITY
       Copying linked Group Policy Objects from one OU to an OU
	
	.PARAMETER
		-SourceOU (requires that the Distinguished Name be supplied)
		-TargetOU (requires that the Distinguished Name be supplied)

    .LINK
		http://www.open-a-socket.com/index.php/2010/12/02/powershell-script-to-copy-gpo-links/#comment-20171
	#>
	param (
		[parameter(Mandatory = $true)]
		[string]$SourceOU,
		[parameter(Mandatory = $true)]
		[string]$TargetOU
	)
	
	# Import the Group Policy module 
	Import-Module GroupPolicy

	# Get the linked GPOs 
	$linked = (Get-GPInheritance -Target $SourceOU).gpolinks
	
	# Loop through each GPO and link it to the target 
	foreach ($link in $linked) {
		$guid = $link.GPOId
		$order = $link.Order
		$enabled = $link.Enabled
		if ($enabled) {
			$enabled = "Yes"
		}
		else {
			$enabled = "No"
		}
		# Create the link on the target 
		New-GPLink -Guid $guid -Target $TargetOU -LinkEnabled $enabled -confirm:$false
		# Set the link order on the target 
		Set-GPLink -Guid $guid -Target $TargetOU -Order $order -confirm:$false
	}
}

function Get-ADDirectReports{
	<#
	.SYNOPSIS
		This function retrieve the directreports property from the IdentitySpecified.
		Optionally you can specify the Recurse parameter to find all the indirect
		users reporting to the specify account (Identity).
	
	.DESCRIPTION
		This function retrieve the directreports property from the IdentitySpecified.
		Optionally you can specify the Recurse parameter to find all the indirect
		users reporting to the specify account (Identity).
	
	.NOTES
		Francois-Xavier Cat
		www.lazywinadmin.com
		@lazywinadm
	
		VERSION HISTORY
		1.0 2014/10/05 Initial Version
	
	.PARAMETER Identity
		Specify the account to inspect
	
	.PARAMETER Recurse
		Specify that you want to retrieve all the indirect users under the account
	
	.EXAMPLE
		Get-ADDirectReports -Identity Test_director
	
		Name                SamAccountName      Mail                Manager
		----                --------------      ----                -------
		test_managerB       test_managerB       test_managerB@la... test_director
		test_managerA       test_managerA       test_managerA@la... test_director
		
	.EXAMPLE
		Get-ADDirectReports -Identity Test_director -Recurse
	
		Name                SamAccountName      Mail                Manager
		----                --------------      ----                -------
		test_managerB       test_managerB       test_managerB@la... test_director
		test_userB1         test_userB1         test_userB1@lazy... test_managerB
		test_userB2         test_userB2         test_userB2@lazy... test_managerB
		test_managerA       test_managerA       test_managerA@la... test_director
		test_userA2         test_userA2         test_userA2@lazy... test_managerA
		test_userA1         test_userA1         test_userA1@lazy... test_managerA
			
	#>
	[CmdletBinding()]
	PARAM (
		[Parameter(Mandatory)]
		[String[]]$Identity,
		[Switch]$Recurse
	)
	BEGIN
	{
		TRY
		{
			IF (-not (Get-Module -Name ActiveDirectory)) { Import-Module -Name ActiveDirectory -ErrorAction 'Stop' -Verbose:$false }
		}
		CATCH
		{
			Write-Verbose -Message "[BEGIN] Something wrong happened"
			Write-Verbose -Message $Error[0].Exception.Message
		}
	}
	PROCESS
	{
		foreach ($Account in $Identity)
		{
			TRY
			{
				IF ($PSBoundParameters['Recurse'])
				{
					# Get the DirectReports
					Write-Verbose -Message "[PROCESS] Account: $Account (Recursive)"
					Get-Aduser -identity $Account -Properties directreports |
					ForEach-Object -Process {
						$_.directreports | ForEach-Object -Process {
							# Output the current object with the properties Name, SamAccountName, Mail and Manager
							Get-ADUser -Identity $PSItem -Properties mail, manager | Select-Object -Property Name, SamAccountName, Mail, @{ Name = "Manager"; Expression = { (Get-Aduser -identity $psitem.manager).samaccountname } }
							# Gather DirectReports under the current object and so on...
							Get-ADDirectReports -Identity $PSItem -Recurse
						}
					}
				}#IF($PSBoundParameters['Recurse'])
				IF (-not ($PSBoundParameters['Recurse']))
				{
					Write-Verbose -Message "[PROCESS] Account: $Account"
					# Get the DirectReports
					Get-Aduser -identity $Account -Properties directreports | Select-Object -ExpandProperty directReports |
					Get-ADUser -Properties mail, manager | Select-Object -Property Name, SamAccountName, Mail, @{ Name = "Manager"; Expression = { (Get-Aduser -identity $psitem.manager).samaccountname } }
				}#IF (-not($PSBoundParameters['Recurse']))
			}#TRY
			CATCH
			{
				Write-Verbose -Message "[PROCESS] Something wrong happened"
				Write-Verbose -Message $Error[0].Exception.Message
			}
		}
	}
	END
	{
		Remove-Module -Name ActiveDirectory -ErrorAction 'SilentlyContinue' -Verbose:$false | Out-Null
	}
}

function New-AESPasswordFile {
	<#	
	.NOTES
	===========================================================================
	 Created on:   	28/03/2017 4:22 PM
	 Created by:   	Angelo Papiccio
	 Organization: 	RACWA
	===========================================================================
	.DESCRIPTION
		This function can be used to create both an AES encryption key file and password file that can be used 
		to pass secure passwords through PowerShell scripts
	.EXAMPLE
		New-AESPasswordFile
		=============================================================================================================================
		Please enter the full path and file name for the AES Key (e.g. C:\AESKey.txt): C:\Temp\AES_OneDrive.txt
		Please enter the password to encrypt: ********************
		Please enter the full path and file name for the Secure Password file (e.g. C:\AppSecurePwd.txt): c:\temp\Secure_OneDrive.txt
		-----------------------------------------------------------------------------------------------------------------------------
	.EXAMPLE
		To use the saved files add the below code to your script
		$UserName = "YOUR USERNAME HERE}
		$SecurePwdFilePath = {PATH TO YOUR PASSWORD FILE HERE}
		$AESKeyFilePath = {PATH TO YOUR AESKEY FILE HERE}
		$AESKey = Get-Content $AESKeyFilePath
		$pwdTxt = Get-Content $SecurePwdFilePath
		$securePwd = $pwdTxt | ConvertTo-SecureString -Key $AESKey
		$credObject = New-Object System.Management.Automation.PSCredential -ArgumentList $username, $securePwd
		You can now pass the $CredObject with the -Credential switch e.g. Connect-MsolService -Credential $CredObject
		
	#>
	
	# Create a 32 bit random key to be used by the AES Key
	$AESKey = New-Object Byte[] 32
	[Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($AESKey)
	
	#Collect the location to store the AES Key file
	$AESKeyFilePath = Read-Host -Prompt "Please enter the full path and file name for the AES Key (e.g. C:\AESKey.txt)"
	Set-Content $AESKeyFilePath $AESKey #It will over-write existing file data if already exists
	
	#Collect the password to encrypt. It uses the -AsSecureString to hide the text then converts it back text and encrypts using the AES Key
	$InputPwd = Read-Host -Prompt "Please enter the password to encrypt" -AsSecureString
	$bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($InputPwd)
	$PlainTxtPsswd = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)
	$secureStringPwd = $PlainTxtPsswd | ConvertTo-SecureString -AsPlainText -Force
	
	#Export Secure content to password file
	$SecurePwdFile = Read-Host -Prompt "Please enter the full path and file name for the Secure Password file (e.g. C:\AppSecurePwd.txt)"
	$password = $secureStringPwd | ConvertFrom-SecureString -Key $AESKey
	Add-Content $SecurePwdFile $password
	
}

function Update-DNS {
	<#	
	.NOTES
	===========================================================================
	 Created on:   	16/05/2017 9:35 AM
	 Created by:   	Angelo Papiccio
	 Organization: 	RACWA
	===========================================================================
	.SYNOPSIS
		A versatile script to allow the updating or creating of DNS records

	.DESCRIPTION
		This function can be run to add a single A record into DNS or it can be run with the ImportCSV switch to use a CSV file to do a bulk update / create.
		
		If the function is run with the -ImportCSV switch (e.g. Update-DNS -ImportCSV) it will prompt you for Administrator credentials and then request that 
		you supply to full path and filename to the CSV (e.g. C:\temp\List.csv)
		
		If the function is run by itself (e.g. Update-DNS) it will prompt you for Administrator credentials before prompting for the new HostName that requires
		updating or creating followed by a prompt for the IP.

		====================================
		FORMAT OF CSV FILE
		====================================
		The CSV file requires two columns with the following headers

		ARecord      IP

	.PARAMETER ImportCSV
		This switch does not require anything to follow it simply denotes that the script will get input via a file

	.EXAMPLE
		Update-DNS

		Description:
		Used when you want to update or create a single A Record

	.EXAMPLE
		Update-DNS -ImportCSV

		Description:
		No file name is entered when running the function with this switch. It is a True or False switch. If True it then runs part of the script that will prompt you for the file path and name of the csv file it will use to import data.

	#>
	param
	(
		[parameter(Mandatory = $false)]
		[Switch]
		$ImportCSV
	)
	
	function UpDateRvrLookUp {
		param
		(
			[parameter(Mandatory = $true)]
			[string]
			$NewIPAddress,
			[Parameter(Mandatory = $true)]
			[string]
			$HostName
		)
		$NewIPAddressArray = $NewIPAddress.Split(".")
		$ZonePrefix = $NewIPAddressArray[0]
		$ReverseZone = $ZonePrefix + ".in-addr.arpa"
		$NewIPAddressFormatted = ($NewIPAddressArray[3] + "." + $NewIPAddressArray[2] + "." + $NewIPAddressArray[1])
		Add-DnsServerResourceRecordPtr -Name "$NewIPAddressFormatted" -zonename $ReverseZone -AllowUpdateAny -PtrDomainName "$HostName.$global:Zone"
	}
	function UpdateDNS {
		param
		(
			[parameter(Mandatory = $true)]
			[string]
			$HostName,
			[Parameter(Mandatory = $true)]
			[string]
			$IPAddress
		)
		$OldARecord = Get-DnsServerResourceRecord -Name "$HostName" -ZoneName "$Global:Zone" -RRType A
		$NewARecord = Get-DnsServerResourceRecord -Name "$HostName" -ZoneName "$Global:Zone" -RRType A
		
		If ([string]::IsNullOrEmpty($OldARecord)) {
			Write-Host "!!! The A Record $HostName does not exist !!!"
			Write-Host -ForegroundColor Green "Adding New A Record for $HostName"
			Add-DnsServerResourceRecordA -Name "$DNSName" -ZoneName "$Global:Zone" -AllowUpdateAny -IPv4Address "$IP"
			UpDateRvrLookUp -HostName "$DNSName" -NewIPAddress "$IP"
		}
		else {
			Write-Host "The A Record $DNSName Exists. Details $ConfirmARecord"
			$NewARecord.RecordData.IPv4Address = [system.Net.IPAddress]::Parse($IP)
			Set-DnsServerResourceRecord -NewInputObject $NewARecord -OldInputObject $OldARecord -zonename "$Global:Zone" -PassThru
			UpDateRvrLookUp -HostName $DNSName -NewIPAddress $IP
		}
	}
	
	Import-Module ActiveDirectory
	

	$Global:Zone = 'rac.com.au'
	
	# Get User Credentials
	$cred = Get-Credential
	
	$Global:ChosenDC = $null
	$ListofDCs = Get-ADDomainController -Filter *
	$OnlineDC = @()
	# This section will test to see if the list od DC's are available. Only the ones that can be pinged will be added to the $OnlineDC array
	foreach ($DC in $ListofDCs) {
		$CheckOnline = $null
		$DCName = $DC.Name
		$CheckOnline = Test-Connection -ComputerName $DCName -Count 2 -Quiet
		if ($CheckOnline -eq "True") {
			$OnlineDC += $DCName
		}
	}
	
	<#
	This section clears the CLI screen and sets loud colours to make sure the users are aware this is a critical script being run
	It will then go through and create a menu based on the available DC's which will be used latter.
	#>
	Clear-Host
	Write-Host "List of Available DNS Servers"
	Write-Host  " "
	for ($i = 0; $i -le $OnlineDC.length - 1; $i++) { "`[{0}]={1}" -f $i, $OnlineDC[$i] }
	Write-Host " "
	[int]$x = Read-Host -Prompt "Please select the DNS server you wish to connect to"
	$Global:ChosenDC = $OnlineDC[$x]
	
	# Connect to remote server using PowerShell Remoting and import the DNSServer Module
	$Session = New-PSSession -ComputerName $Global:ChosenDC -Credential $cred
	Import-PSSession -Session $Session -Module DNSServer -AllowClobber
	
	if ($ImportCSV -eq $false) {
		Write-Host " "
		$DNSName = Read-Host "Please enter the new A Record for DNS"
		$IP = Read-Host "Please enter the IP address for the A Record"
		UpdateDNS -HostName $DNSName -IPAddress $IP
	}
	else {
		Write-Host " "
		$CSVFile = Read-Host "Please enter the path and file name of the csv file (e.g. c:\temp\list.csv)"
		$DNSList = Import-Csv $CSVFile
		foreach ($ARecord in $DNSList) {
			$DNSName, $ConfirmARecord, $IP, $OldARecord, $NewARecord = $null
			$DNSName = $ARecord.Arecord
			$IP = $ARecord.IP
			UpdateDNS -HostName $DNSName -IPAddress $IP
		}		
	}
}


Export-ModuleMember -Function 'Get*'
Export-ModuleMember -Function 'Copy*'
Export-ModuleMember -Function 'Create*'
Export-ModuleMember -Function 'Update*'